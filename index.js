const moment = require('moment');
const fs = require('fs');

// read the input file into a string
// assumptions: input file is valid and readings are sorted by timestamp ascending
const input = fs.readFileSync('./input', { encoding: 'utf8'} );
// split into lines on the newline char
const lines = input.split('\n');

// utility function to remove any readings more than 5 minutes before the timestamp of currReading
function pruneOldReadings(readings, currReading) {
    // create a new date and set it to be 5 minutes prior to the current reading
    const targetDate = currReading.timestamp.clone();
    targetDate.subtract(5, "minutes");
    // filter out readings that are before the target date
    return readings.filter(r => r.timestamp.isSameOrAfter(targetDate));
}

// Component base class to organize common component functionality
class Component {
    constructor(componentName, componentSeverity) {
        // readings will be a hash of satelliteIds. Each id is mapped to an array of 
        // readings for that sattelite 
        this.readings = {};
        this.componentName = componentName;
        this.componentSeverity = componentSeverity;
    }
    
    addReading(reading) {
        // make sure an array exists for reading.satelliteId
        if (!this.readings[reading.satelliteId]) {
            this.readings[reading.satelliteId] = [];
        }
        const satReadings = this.readings[reading.satelliteId];
        // Component subclasses are expected to define an isOutOfRange method
        if (this.isOutOfRange(reading)) {
            // if reading is out of desired range, add to readings array
            satReadings.push(reading);
            // remove readings that are no longer within 5 minutes of the parameter reading.
            const filteredReadings = pruneOldReadings(satReadings, reading);
            this.setSatReadings(filteredReadings);
        } 
    }
    setSatReadings(satelliteId, newReadings) {
        this.readings[satelliteId] = newReadings;
    }
    getAlert(satelliteId) {
        const satReadings = this.readings[satelliteId];
        if (satReadings.length >= 3) {
            // use the earliest reading of the 5 minute range for the timestamp of the alert
            // (based on the expected output in the README)
            const alertTimestamp = satReadings[0].timestamp;
            return {
                "satelliteId": satelliteId, 
                "severity": this.componentSeverity,
                "component": this.componentName,
                "timestamp": moment(alertTimestamp).format("YYYY-MM-DDTHH:mm:ss.SSS") + "Z"
            }
        }
    }
}

class Battery extends Component {
    constructor() {
        super("BATT", "RED LOW");
    }
    isOutOfRange(reading) {
        return (reading.rawValue < reading.redLowLimit);
    }
}

class Thermostat extends Component {
    constructor() {
        super("TSTAT", "RED HIGH");
    }
    isOutOfRange(reading) {
        return (reading.rawValue > reading.redHighLimit);
    }
}

const battery = new Battery();
const thermostat = new Thermostat();
const output = [];

const getComponentFromString = (name) => {
    if (battery.componentName === name) {
        return battery;
    }
    if (thermostat.componentName === name) {
        return thermostat;
    }
    console.log('Unknown component:', name);
    return;
}

const convertLineToReading = (line) => {
    const fields = line.split("|");
    return {
        timestamp: moment(fields[0], "YYYYMMDD hh:mm:ss.SSS"),
        satelliteId: parseInt(fields[1]),
        redHighLimit: parseFloat(fields[2]),
        yellowHighLimit: parseFloat(fields[3]),
        yellowLowLimit: parseFloat(fields[4]),
        redLowLimit: parseFloat(fields[5]),
        rawValue: parseFloat(fields[6]),
        component: fields[7]   
    };
}

// main loop:
// iterate over lines and add readings to components, generating an alert if needed.
for (const line of lines) {
    // convert line to reading object
    const reading = convertLineToReading(line);
    // get component instance for reading
    const component = getComponentFromString(reading.component);
    // add the reading to the component
    component.addReading(reading);
    // check if an alert has been triggered for the satellite of the current reading
    const alert = component.getAlert(reading.satelliteId);
    if (alert) {
        output.push(alert);  
    } 
}

console.log(output);
